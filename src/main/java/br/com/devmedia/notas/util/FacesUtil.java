package br.com.devmedia.notas.util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.List;

public class FacesUtil {

    private static void addMessage(FacesMessage.Severity severity, String summary, String detail, String client) {
        FacesContext.getCurrentInstance().
                addMessage(client, new FacesMessage(severity, summary, detail));
    }

    public static void showInfo(String mensagem, String client) {
        addMessage(FacesMessage.SEVERITY_INFO, "Informação", mensagem, client);
    }

    public static void showWarn(String mensagem, String client) {
        addMessage(FacesMessage.SEVERITY_WARN, "Atenção", mensagem, client);
    }

    public static void showError(String mensagem, String client) {
        addMessage(FacesMessage.SEVERITY_ERROR, "Erro", mensagem, client);
    }

    public static void showMultipleErrors(List<String> messages, String client) {
        messages.forEach(p-> showError(p, client));
    }

    public static void showMultipleInfos(List<String> messages, String client) {
        messages.forEach(p-> showInfo(p, client));
    }

    public static void showMultipleWarnings(List<String> messages, String client) {
        messages.forEach(p-> showWarn(p, client));
    }
}
