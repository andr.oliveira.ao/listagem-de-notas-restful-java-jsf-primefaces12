package br.com.devmedia.notas.managedbean;

import br.com.devmedia.notas.model.Nota;
import br.com.devmedia.notas.rest.NotaREST;
import br.com.devmedia.notas.util.FacesUtil;
import lombok.Getter;
import lombok.Setter;
import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DialogFrameworkOptions;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import java.io.Serializable;
import java.util.*;

@Getter
@Setter
@Named
@SessionScoped
public class NotaBean implements Serializable {

    private static final long serialVersionUID = -1009316379646237699L;

    private final NotaREST notaREST = new NotaREST();

    private List<Nota> listaNotas;
    private Nota nota;
    private SelectEvent event;
    private String descricaoTelaEdicao;

    @PostConstruct
    private void init() {
        listarNotas();
    }

    public void listarNotas() {
        try {
            listaNotas = notaREST.listar();
        } catch (Exception e) {
            FacesUtil.showError("Erro ao carregar as notas", null);
        }
    }

    public void initDetalhes(Nota nota) {
        descricaoTelaEdicao = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("descDialogEdicao");

        if (nota != null) {
            this.nota = notaREST.obter(nota.getId());
        } else {
            this.nota = new Nota();
        }
    }

    public void salvarEdicao() {
        try {
            if (nota.getId() != null) {
                notaREST.atualizar(nota);
            } else {
                notaREST.inserir(nota);
            }

            listarNotas();
            PrimeFaces.current().dialog().closeDynamic(Boolean.TRUE);
        } catch (Exception e) {
            FacesUtil.showError("Erro ao salvar a nota", null);
        }
    }

    public void editarNota(Nota nota) {
        FacesContext.getCurrentInstance().getExternalContext().getFlash()
                .put("descDialogEdicao", Objects.nonNull(nota) ? "Editar Nota" : "Cadastrar Nota");

        DialogFrameworkOptions options = DialogFrameworkOptions.builder()
                .modal(true)
                .fitViewport(true)
                .responsive(true)
                .width("900px")
                .contentWidth("100%")
                .resizeObserver(true)
                .resizeObserverCenter(true)
                .resizable(false)
                .styleClass("max-w-screen")
                .iframeStyleClass("max-w-screen")
                .build();

        initDetalhes(nota);

        PrimeFaces.current().dialog().openDynamic("editarNota", options, null);
    }

    public void onReturnFromEdicaoCadastroNota(SelectEvent event) {
        if (Objects.nonNull(event.getObject()) && Boolean.TRUE.equals(event.getObject())) {
            FacesUtil.showInfo("Nota salva com sucesso", null);
        }
    }

    public void exibirDetalhes(Nota nota) {
        DialogFrameworkOptions options = DialogFrameworkOptions.builder()
                .modal(true)
                .fitViewport(true)
                .responsive(true)
                .width("900px")
                .contentWidth("100%")
                .resizeObserver(true)
                .resizeObserverCenter(true)
                .resizable(false)
                .styleClass("max-w-screen")
                .iframeStyleClass("max-w-screen")
                .onHide("updateGrid()")
                .build();

        initDetalhes(nota);

        PrimeFaces.current().dialog().openDynamic("detalheNota", options, null);
    }

    public void excluirNota(Nota nota, Boolean fecharDialog) {
        try {
            notaREST.excluir(nota.getId());

            listarNotas();

            if (fecharDialog) {
                PrimeFaces.current().dialog().closeDynamic(null);
            }

            FacesUtil.showInfo("Nota excluída com sucesso", null);

        } catch (Exception e) {
            FacesUtil.showError("Erro ao excluir a nota", null);
        }
    }
}
