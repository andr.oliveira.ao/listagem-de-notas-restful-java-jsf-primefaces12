package br.com.devmedia.notas.rest;

import br.com.devmedia.notas.model.Nota;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

public class NotaREST {

    private static final String REST_URI = "http://www.deveup.com.br/notas/api/";

    private final Client client;

    public NotaREST(){
        client = ClientBuilder.newClient();
    }

    public List<Nota> listar() {
        return client.target(REST_URI)
                .path("notes")
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<ArrayList<Nota>>(){});
    }

    public Nota obter(Long id) {
        return client.target(REST_URI)
                .path("notes").path(id.toString())
                .request(MediaType.APPLICATION_JSON)
                .get(new GenericType<Nota>(){});
    }

    public void atualizar(Nota nota) {
        Invocation.Builder invocationAtualizar = client.target(REST_URI)
                .path("notes").path(nota.getId().toString())
                .request(MediaType.APPLICATION_JSON);

        try (Response response = invocationAtualizar.put(Entity.entity(nota, MediaType.APPLICATION_JSON))) {
            if (response.getStatus() != Response.Status.OK.getStatusCode()) {
                throw new RuntimeException("Erro ao atualizar a nota");
            }
        }
    }

    public Nota inserir(Nota nota) {
        Invocation.Builder invocationInserir = client.target(REST_URI)
                .path("notes")
                .request(MediaType.APPLICATION_JSON);

        try (Response response = invocationInserir.post(Entity.entity(nota, MediaType.APPLICATION_JSON))) {
            if (response.getStatus() != Response.Status.OK.getStatusCode()) {
                throw new RuntimeException("Erro ao inserir a nota");
            }
            return response.readEntity(Nota.class);
        }
    }

    public void excluir(Long id) {
        Invocation.Builder invocationAtualizar = client.target(REST_URI)
                .path("notes").path(id.toString())
                .request(MediaType.APPLICATION_JSON);

        try (Response response = invocationAtualizar.delete()) {
            if (response.getStatus() != Response.Status.OK.getStatusCode()) {
                throw new RuntimeException("Erro ao excluir a nota");
            }
        }
    }
}
