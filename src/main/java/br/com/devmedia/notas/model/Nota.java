package br.com.devmedia.notas.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Nota {
    private Long id;
    private String title;
    private String body;
}
